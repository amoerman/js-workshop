var myGreeting = "HELLO WORLD";
var greeting = "HELLO UNIVERSE";

/* VERY commonly used in libraries to prevent leaks and to have some form of dependency injection */
(function(greeting) {
    console.log("1. greeting inside the IIFE: " + greeting);
    greeting = "GOODBYE UNIVERSE";
})(myGreeting);

console.log("2. myGreeting outside the IIFE: " + myGreeting);
console.log("3. greeting outside the IIFE: " + greeting);
