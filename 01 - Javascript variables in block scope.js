try {
    console.log(`1. x outside the if block: ${x}`);
} catch(error) {
    console.log("1. Reference error for x outside the if block");
}

if(true) {
    console.log(`2. x inside the if block, before it is declared: ${x}`);

    var x = 17;

    console.log(`3. x inside the if block, after it is declared: ${x}`);
}

try {
    console.log(`4. x outside the if block, after it is declared: ${x}`);
} catch(error) {
    console.log("4. Reference error for x outside the if block, after it is declared");
}