try {
    console.log(`1. doSomething() outside the if block: ${doSomething}`);
} catch(error) {
    console.log(`1. Reference error for doSomething() outside the if block: ${doSomething}`);
}
if(true) {
    console.log(`2. doSomething() inside the if block, before it is declared: ${doSomething}`);

    function doSomething() { }

    console.log(`3. doSomething() inside the if block, after it is declared: ${doSomething}`);
}

try {
    console.log(`4. doSomething() outside the if block, after it is declared: ${doSomething}`);
} catch(error) {
    console.log(`4. Reference error for doSomething() outside the if block, after it is declared: ${doSomething}`);
}
