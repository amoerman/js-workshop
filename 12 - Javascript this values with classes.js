(function() {
    this.name = "A silly penguin";
    class Greeter {
        constructor() {
            this.name = "Jack";
            this.sayGoodbye = () => "Goodbye, I was " + this.name;
        }
        sayHello() {
            return "Hi there, I am " + this.name;
        }
    }
    const greeter = new Greeter();
    console.log("1. Calling a method on a class: " + greeter.sayHello());
    console.log("2. Calling a lambda on a class: " + greeter.sayGoodbye());
    try {
        const sayHello = greeter.sayHello;
        console.log("3. Calling a captured method on a class: " + sayHello());
    } catch(error) {
        console.log("3. Error trying to call a captured method: " + error);
    }
    try {
        const sayGoodbye = greeter.sayGoodbye;
        console.log("4. Calling a captured lambda on a class: " + sayGoodbye());
    } catch(error) {
        console.log("4. Error trying to call a captured lambda: " + error);
    }
    // not the same behavior if you transpile to ES5!!!
})();
