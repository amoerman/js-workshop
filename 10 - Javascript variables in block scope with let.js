let a = 0;
if(true){
    let a = 2;
    let b = 10;
    console.log("1. here a = ",a);
    console.log("2. here b = ",b);
}
console.log("3. at the end a = ",a);

try {
    console.log("4. at the end b = ",b);
}
catch(error) {
    console.log("4. Reference error for 'b' outside the if scope");
}
