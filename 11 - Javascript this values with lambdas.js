(function() {
    this.theHolyGrail = "A fake holy grail, muhahaha!";
    console.log("");
    console.log("*** Using an object literal with a function ***");
    var guardian1 = {
        theHolyGrail: "The authentic holy grail",
        getHolyGrail: function () {
            return this.theHolyGrail;
        }
    };
    console.log(`1. Calling a function on an object literal: ${guardian1.getHolyGrail()}`);
    var capturedGetHolyGrail = guardian1.getHolyGrail;
    console.log(`2. Calling a captured function on an object literal: ${capturedGetHolyGrail()}`);
    console.log("");


    console.log("*** Using a constructor function with a function ***");
    function Guardian2() {
        this.theHolyGrail = "The authentic holy grail";
        this.getHolyGrail = function () {
            return this.theHolyGrail;
        };
    }
    var guardian2 = new Guardian2();
    console.log(`3. Calling a function on an object instance: ${guardian2.getHolyGrail()}`);
    capturedGetHolyGrail = guardian2.getHolyGrail;
    console.log(`4. Calling a captured function on an object instance ${capturedGetHolyGrail()}`);
    console.log("");
    console.log("*** Using an object literal with a lambda ***");
    var guardian3 = {
        theHolyGrail: "The authentic holy grail",
        getHolyGrail: () => this.theHolyGrail
    };
    console.log(`5. Calling a lambda on an object literal: ${guardian3.getHolyGrail()}`);
    capturedGetHolyGrail = guardian3.getHolyGrail;
    console.log(`6. Calling a captured lambda on an object literal ${capturedGetHolyGrail()}`);
    console.log("");
    console.log("*** Using a constructor function with a lambda ***");
    function Guardian4() {
        this.theHolyGrail = "The authentic holy grail";
        this.getHolyGrail = () => this.theHolyGrail;
    }
    var guardian4 = new Guardian4();
    console.log(`7. Calling a lambda on an object instance: ${guardian4.getHolyGrail()}`);

    capturedGetHolyGrail = guardian4.getHolyGrail;
    console.log(`8. Calling a captured lambda on an object instance: ${capturedGetHolyGrail()}`);
})();