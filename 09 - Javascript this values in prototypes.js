(function () {
    function Greeter() {
        this.name = "Alex";
        this.sayHello = function () {
            return `Hello there, I am ${this.name}`;
        }
    }

    var greeter1 = new Greeter();
    Greeter.prototype.name = "Jesse";
    Greeter.prototype.sayHello = function () { return "Ehm no, I am " + this.name; };
    Greeter.prototype.sayGoodbye = function () { return "Goodbye, I was " + this.name;  };
    var greeter2 = new Greeter();

    console.log(`1. Greeter 1: ${greeter1.sayHello()}`);
    console.log(`2. Greeter 2: ${greeter2.sayHello()}`);

    try {
        console.log(`3. Greeter 1: ${greeter1.sayGoodbye()}`);
    } catch(error) {
        console.log("3. Greeter 1: Error trying to say goodbye : " + error);
    }

    try {
        console.log(`4. Greeter 2: ${greeter2.sayGoodbye()}`);
    } catch(error) {
        console.log("4. Greeter 2: Error trying to say goodbye : " + error);
    }
})();
