(function () {
    this.name = "A silly penguin";
    var Greeter = (function () {
        function Greeter() {
            var _this = this;
            this.name = "Jack";
            this.sayGoodbye = function () { return "Goodbye, I was " + _this.name; };
        }
        Greeter.prototype.sayHello = function () {
            return "Hi there, I am " + this.name;
        };
        return Greeter;
    }());
    var greeter = new Greeter();
    console.log("1. " + greeter.sayHello());
    console.log("2. " + greeter.sayGoodbye());
    try {
        var sayHello = greeter.sayHello;
        console.log("3. " + sayHello());
    }
    catch (error) {
        console.log("3. Error trying to say hello with the captured function: " + error);
    }
    try {
        var sayGoodbye = greeter.sayGoodbye;
        console.log("4. " + sayGoodbye());
    }
    catch (error) {
        console.log("4. Error trying to say goodbye with the captured function: " + error);
    }
    // not the same behavior if you transpile to ES5!!!
})();
