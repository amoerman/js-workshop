try {
    console.log(`1. x before the function is called: ${x}`);
} catch(error) {
    console.log(`1. Reference error before the function is called: ${error}`);
}

function doSomething() {
    console.log(`2. x inside the function, before the declaration: ${x}`);

    var x = 12;

    console.log(`3. x inside the function, after the declaration: ${x}`);
}

doSomething();

try {
    console.log(`4. x after the function is called: ${x}`);
} catch(error) {
    console.log(`4. Reference error after the function is called: ${error}`);
}