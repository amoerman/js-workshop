1. Error trying to say hello before the naughty greeter TypeError: this.sayHello is not a function
2. greeterInstance.sayHello with simple function invocation: Hello there, I am Mark!
3. greeterInstance.sayHello with binding: Hello there, I am Jeff!
4. greeterInstance.sayHello with apply: Hello there, I am Jeff!
5. greeterInstance.sayHello with call: Hello there, I am Jeff!
6. Captured sayHello() from greeter instance: Hello there, I am Alex!
7. Captured sayHello() from greeter instance and bound again: Hello there, I am Mark!
8. naughtyGreeter.sayHello with simple function invocation: Hello there, I am Mark!
9. this.name after the naughty greeter: Mark
10. this.sayHello after the naughty greeter: Hello there, I am Mark!
