(function() {
    this.name = "Alex";

    var greeterObject = {
        name : "Jack",
        sayHelloFromObject: function() {
            return `Hello there, I am ${this.name}!`;
        }
    };

    console.log("1. greeterObject.sayHelloFromObject with simple function invocation: "
        + greeterObject.sayHelloFromObject());

    console.log("2. greeterObject.sayHelloFromObject with binding: "
        + greeterObject.sayHelloFromObject.bind({ name: "Jeff" })());

    console.log("3. greeterObject.sayHelloFromObject with apply: "
        + greeterObject.sayHelloFromObject.apply({ name: "Jeff" }));

    console.log("4. greeterObject.sayHelloFromObject with call: "
        + greeterObject.sayHelloFromObject.call({ name: "Jeff" }));

    var sayHello = greeterObject.sayHelloFromObject;
    console.log("5. Captured sayHello from greeter object: " + sayHello());

    console.log("6. Captured sayHello from greeter object and bound again: " + sayHello.bind(greeterObject)());
})();
