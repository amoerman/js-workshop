// Recommendation 1: Always use const or let, they are much more predictable

const something = "abc";
let somethingElse = "def";

// Recommendation 2: Always use lambdas instead of functions,
// they are much more predictable and will usually give you the correct "this" value

const someFunction = () => { };

// Recommendation 3: Don't put functions on object literals, the scoping rules there are just confusing
// Use object literals only for what they were intended to be: value objects with properties (deserialized JSON, etc.)
const someObject = { name : "Jeff" };

// Recommendation 4: Use classes / constructor functions with prototypes to create behavior
// If you use constructor functions, guard against incorrect usage by checking the type of the "this" value
class GreeterClass {
    constructor() {
        this.name = "Jack";
    }
    sayHello() { return "Hello there! I am " + this.name }
}

function GreeterConstructorFunction() {
    if(!(this instanceof GreeterConstructorFunction)) return new GreeterConstructorFunction();
    this.sayHello = () => {};
}

const greeter1 = new GreeterClass();
const greeter2 = new GreeterConstructorFunction();

// Recommendation 5: favor writing out the lambda if you register a callback,
// it prevents a lot of problems

 //onClick={() => this.doTheThing}

const callbacks = [];
const registerCallback = callback => callbacks.push(callback);
// DON'T
registerCallback(greeter2.sayHello);
// DO
registerCallback(() => greeter2.sayHello());

