(function() {
    this.name = "Alex";

    try {
        console.log("1. this.sayHello before the naughty greeter: " + this.sayHello());
    } catch(error) {
        console.log("1. Error trying to say hello before the naughty greeter " + error);
    }

    function Greeter() {
        this.name = "Mark";

        this.sayHello = function() {
            return `Hello there, I am ${this.name}!`;
        };
        return this; // is the default behavior, but it's better to be explicit about it
    }

    var greeterInstance = new Greeter();
    console.log("2. greeterInstance.sayHello with simple function invocation: " + greeterInstance.sayHello());
    console.log("3. greeterInstance.sayHello with binding: " + greeterInstance.sayHello.bind({ name: "Jeff" })());
    console.log("4. greeterInstance.sayHello with apply: " + greeterInstance.sayHello.apply({ name: "Jeff" }));
    console.log("5. greeterInstance.sayHello with call: " + greeterInstance.sayHello.call({ name: "Jeff" }));

    /* Every typed onClick={this.handleClick} in a React component and it didn't work? Here's why :-) */
    var capturedSayHelloFromGreeterInstance = greeterInstance.sayHello;
    console.log("6. Captured sayHello() from greeter instance: "
        + capturedSayHelloFromGreeterInstance());  // you won't believe what's going on here

    console.log("7. Captured sayHello() from greeter instance and bound again: "
        + capturedSayHelloFromGreeterInstance.bind(greeterInstance)());

    var naughtyGreeter = Greeter(); // huh, where did the new go?
    console.log("8. naughtyGreeter.sayHello with simple function invocation: " + naughtyGreeter.sayHello());
    console.log("9. this.name after the naughty greeter: " + this.name);
    console.log("10. this.sayHello after the naughty greeter: " + this.sayHello());
})();
