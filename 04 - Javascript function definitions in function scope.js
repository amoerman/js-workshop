try {
    console.log(`1. inner() outside outer(): ${inner}`);
} catch(error) {
    console.log(`1. Reference error before outer(): ${error}`);
}

outer();

function outer() {
    console.log(`2. inner() inside outer(): ${inner}`);

    function inner() { }

    console.log(`3. inner() inside outer(), after the declaration: ${inner}`);
}

try {
    console.log(`4. inner() outside outer() after the declaration: ${inner}`);
} catch(error) {
    console.log(`4. Reference error outside outer() after the declaration: ${error}`);
}