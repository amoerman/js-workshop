(function () {
    this.name = "Alex";

    function sayHello() {
        return `Hello there, I am ${this.name}!`;
    }

    console.log("1. sayHello with simple function invocation: " + sayHello());
    console.log("2. sayHello with binding: " + sayHello.bind({name: "Jeff"})());
    console.log("3. sayHello with apply: " + sayHello.apply({name: "Jeff"}));
    console.log("4. sayHello with call: " + sayHello.call({name: "Jeff"}));
})();

